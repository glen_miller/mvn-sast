# Maven SAST, Code Quality, and Releases Project

This was originally for setting up SAST and Code Quality on a Maven project. Now, it has expanded to include work around [Releases](https://docs.gitlab.com/ee/user/project/releases)

## Release Form

The pipeline is setup to create a new release every time a tag is applied. It does the following:

* Does a fake build
* Creates some artifacts to link to the release
  * Creates a zip file to use as a release artifact
  * Builds a container and saves it as a tarball, also to use as a release artifact
  * Builds a jar file also to use as a release artifact
* Runs SAST (for Java and JavaScript) as well as Code Quality
* Creates the actual release and attaches the container tarball, JAR, and file zip as linked artifacts
  * https://docs.gitlab.com/ee/user/project/releases/#use-a-generic-package-for-attaching-binaries
  * You could also upload these to external storage and provide that link
* Note: The SAST and Code Quality artifacts are automatically attached as they are tagged not just with `artifacts:reports` but also by explicit `artifacts:paths`
  * https://docs.gitlab.com/ee/user/project/releases/#include-report-artifacts-as-release-evidence

The results for quering the release after creation are below, but let's discuss them, first:

* The code is automatically linked as four different archive types. This happens when a release is created
* The `evidence` for this release is created. This `evidence` automatically includes links to the artifacted SAST and Code Quality reports
* The links to the file zip, tarball, and JAR that were added in the pipeline when the release was created are shown in the `assets:links` section of the JSON

Here is the results of an API call to get the release around the tag `sixth_tag`

First, the command (note: PRIVATE-TOKEN header not shown)

```bash
curl --request GET --url https://gitlab.com/api/v4/projects/23785535/releases/sixth_tag --header 'PRIVATE-TOKEN: SOMETOKEN'
```

The results

```json
{
  "name": "Release sixth_tag",
  "tag_name": "sixth_tag",
  "description": null,
  "created_at": "2021-09-04T17:57:57.556Z",
  "released_at": "2021-09-04T17:57:57.556Z",
  "author": {
    "id": 4090207,
    "name": "Glen P Miller",
    "username": "glen_miller",
    "state": "active",
    "avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/4090207/avatar.png",
    "web_url": "https://gitlab.com/glen_miller"
  },
  "commit": {
    "id": "24a92c2814b00ebe32a67803f8d2f364e7250335",
    "short_id": "24a92c28",
    "created_at": "2021-09-04T17:10:21.000+00:00",
    "parent_ids": [
      "f09c114c63fc8c8aedc3744381f762acc784d2f5"
    ],
    "title": "Update .gitlab-ci.yml",
    "message": "Update .gitlab-ci.yml",
    "author_name": "Glen P Miller",
    "author_email": "gmiller@gitlab.com",
    "authored_date": "2021-09-04T17:10:21.000+00:00",
    "committer_name": "Glen P Miller",
    "committer_email": "gmiller@gitlab.com",
    "committed_date": "2021-09-04T17:10:21.000+00:00",
    "trailers": {},
    "web_url": "https://gitlab.com/glen_miller/mvn-sast/-/commit/24a92c2814b00ebe32a67803f8d2f364e7250335"
  },
  "upcoming_release": false,
  "commit_path": "/glen_miller/mvn-sast/-/commit/24a92c2814b00ebe32a67803f8d2f364e7250335",
  "tag_path": "/glen_miller/mvn-sast/-/tags/sixth_tag",
  "assets": {
    "count": 7,
    "sources": [
      {
        "format": "zip",
        "url": "https://gitlab.com/glen_miller/mvn-sast/-/archive/sixth_tag/mvn-sast-sixth_tag.zip"
      },
      {
        "format": "tar.gz",
        "url": "https://gitlab.com/glen_miller/mvn-sast/-/archive/sixth_tag/mvn-sast-sixth_tag.tar.gz"
      },
      {
        "format": "tar.bz2",
        "url": "https://gitlab.com/glen_miller/mvn-sast/-/archive/sixth_tag/mvn-sast-sixth_tag.tar.bz2"
      },
      {
        "format": "tar",
        "url": "https://gitlab.com/glen_miller/mvn-sast/-/archive/sixth_tag/mvn-sast-sixth_tag.tar"
      }
    ],
    "links": [
      {
        "id": 328170,
        "name": "files.tar.gz",
        "url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/files.tar.gz",
        "direct_asset_url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/files.tar.gz",
        "external": true,
        "link_type": "other"
      },
      {
        "id": 328169,
        "name": "myimage.tar.gz",
        "url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/myimage.tar.gz",
        "direct_asset_url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/myimage.tar.gz",
        "external": true,
        "link_type": "other"
      },
      {
        "id": 328168,
        "name": "demo-0.0.1-SNAPSHOT.jar",
        "url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/demo-0.0.1-SNAPSHOT.jar",
        "direct_asset_url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/demo-0.0.1-SNAPSHOT.jar",
        "external": true,
        "link_type": "other"
      }
    ]
  },
  "evidences": [
    {
      "sha": "b7fc1a4334d6df9cf17d30b939c190d4babf9240892d",
      "filepath": "https://gitlab.com/glen_miller/mvn-sast/-/releases/sixth_tag/evidences/1425308.json",
      "collected_at": "2021-09-04T17:57:57.649Z"
    }
  ],
  "_links": {
    "self": "https://gitlab.com/glen_miller/mvn-sast/-/releases/sixth_tag",
    "edit_url": "https://gitlab.com/glen_miller/mvn-sast/-/releases/sixth_tag/edit"
  }
}
```

## Import/Export

When we export, there is a `releases.ndjson` in the `tree:project` section of the export zip file. However, this, oddly, doesn't contain the `evidence` but *does* include the secondary artifact links we added when we created the release.

### Other items of note

* The import process *seems* to just regenerate the releases, since the tars of source code, etc, don't seem to live in the export
* The `evidence` is not exported as part of this, so any true import/export process will need to go grab this, and upload it to the import
* Alternately, you could pull the evidence file and add it as an additional link

```json
{
  "id": 3386391,
  "tag": "sixth_tag",
  "description": null,
  "project_id": 23785535,
  "created_at": "2021-09-04T17:57:57.556Z",
  "updated_at": "2021-09-04T17:57:57.654Z",
  "author_id": 4090207,
  "name": "Release sixth_tag",
  "sha": "24a92c2814b00ebe32a67803f8d2f364e7250335",
  "released_at": "2021-09-04T17:57:57.556Z",
  "links": [
    {
      "id": 328168,
      "url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/demo-0.0.1-SNAPSHOT.jar",
      "name": "demo-0.0.1-SNAPSHOT.jar",
      "created_at": "2021-09-04T17:57:57.561Z",
      "updated_at": "2021-09-04T17:57:57.561Z",
      "filepath": null,
      "link_type": "other"
    },
    {
      "id": 328170,
      "url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/files.tar.gz",
      "name": "files.tar.gz",
      "created_at": "2021-09-04T17:57:57.565Z",
      "updated_at": "2021-09-04T17:57:57.565Z",
      "filepath": null,
      "link_type": "other"
    },
    {
      "id": 328169,
      "url": "https://gitlab.com/api/v4/projects/23785535/packages/generic/release_sixth_tag/sixth_tag/myimage.tar.gz",
      "name": "myimage.tar.gz",
      "created_at": "2021-09-04T17:57:57.563Z",
      "updated_at": "2021-09-04T17:57:57.563Z",
      "filepath": null,
      "link_type": "other"
    }
  ]
}
```

## Regnerating Evidence

I had thought that, after export/import, you might be able to regenerate evidence by:

* Running a new pipeline for the tag to regenerate artifacts
* Triggering evidence collection via the API

